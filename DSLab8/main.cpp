#include <iostream>
#include <algorithm>
#include "BinaryTree.h"

using namespace std;

const int SIZE = 15;
int NUMS[SIZE] = { 49, 28, 83, 18, 40, 71, 97, 11, 19, 32, 44, 69, 72, 92, 99 };

int getInt() {
	int num;

	cin >> num;

	// Validation
	while (cin.fail()) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "Enter a valid integer: ";
		cin >> num;
	}

	return num;
}

int getPosInt() {
	int num;

	cin >> num;

	// Validation
	while (cin.fail() || num < 0) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "Enter a valid integer >= 0: ";
		cin >> num;
	}

	return num;
}

// commented out the code allowing user to select numbers
int main() {
	//int size;
	//int* nums;

	// come on and slam, and
	//cout << "Welcome to the tree viewer.\n Enter the size of the tree: " << endl;
	//size = getPosInt();

	//nums = new int[size];

	//for (int i = 0; i < size; i++) {
	//	cout << "Enter next number: ";
	//	nums[i] = getInt();
	//	cout << endl;
	//}

	sort(NUMS, NUMS + SIZE);

	BinaryTree tree = BinaryTree(NUMS, SIZE);
	tree.printTree();
	return 0;
}