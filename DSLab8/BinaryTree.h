#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <iostream>

using namespace std;

struct Node {
	int* data = nullptr;
	Node* left = nullptr;
	Node* right = nullptr;
};


class BinaryTree
{
private:
	Node* root;

public:
	BinaryTree() {
		root = nullptr;
	}

	// assumes a sorted array
	BinaryTree(int arr[], int size) {
		if (size <= 0) {
			this->root = nullptr;
			return;
		}

		this->root = this->buildTree(arr, size);
	}

	Node* buildTree(int arr[], int size) {
		// base case: size of subtree is only 1 or 0
		if (size == 0) {
			return nullptr;
		}

		if (size == 1) { 
			return new Node{ new int(arr[0]), nullptr, nullptr };
		}

		// middle index if size is odd, right of the middle two indices if even
		int midpoint = (size / 2);

		Node* node = new Node{ new int(arr[midpoint]), nullptr, nullptr };


		// size of left subtree always = midpoint
		node->left = buildTree(arr, midpoint);

		// size of the right subtree = midpoint if odd, but it = midpoint + 1 if even
		node->right = buildTree(&arr[midpoint + 1], midpoint - 1 + (size % 2));

		return node;
	}

	// calls the recursive print tree function
	void printTree() {
		printTreeRec(this->root, 0);
	}

	void printTreeRec(Node* subtree, int numSpaces) {
		// base case: subtree == nullptr
		if (!subtree) {
			return;
		}

		// print right subtree first, add 5 spaces per level
		printTreeRec(subtree->right, numSpaces + 5);

		// print spaces
		for (int i = 0; i < numSpaces; i++) {
			cout << " ";
		}

		// print data
		cout << *subtree->data << endl;

		// print left subtree, add 5 spaces per level
		printTreeRec(subtree->left, numSpaces + 5);
	}
};

#endif
